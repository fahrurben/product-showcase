import React from 'react';
import ReactDOM from 'react-dom';
import "babel-polyfill";

import { fetchHelper, sleep } from './commonHelper';
import { apiUrl } from './constant';

var addsInterval = 19;                              // Interval for showing ads

class ProductPage extends React.Component {

  constructor(props) {
    super();

    this.initalState = {
      products: [],
      ads: [],
      page: 1,
      pageLoaded: 1,
      size: 20,
      sortBy: 'id',
      total: 0,
      isLoading: false,
      isLoadingCache: false, 
      hasMore: true
    }
    this.state = { ...this.initalState };

    this.sortOptions = [
      { value: 'id', label: 'ID' },
      { value: 'size', label: 'Size' },
      { value: 'price', label: 'Price' }
    ];

    this.doSort = this.doSort.bind(this);
    this.loadCache = this.loadCache.bind(this);
  }

  async componentDidMount() {
    document.addEventListener('scroll', this.trackScrolling);
    await this.loadInitialsProducts();

    // Creating interval timer to load products when idle
    this.loadInterval = setInterval(this.loadCache, 500);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
    this.loadInterval = null;
  }

  // Convert string to date
  static strToDate(str) {
    let dateParts = str.split(' ');
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const month = months.findIndex((mon) => mon === dateParts[1]);
    let timeParts = dateParts[4].split(':');
    let date = new Date(dateParts[3], month, dateParts[2], timeParts[0], timeParts[1], timeParts[2]); 
    return date;
  }

  static dateToStr(date) {
    return (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
  }

  // Get different day of two date
  static diffDayOfDate(now, date) {
    // Get different day of to date, 1000 * 3600 * 24 is total miliseconds of one day 
    return Math.ceil((now.getTime() - date.getTime()) / (1000 * 3600 * 24));
  }

  // Initialize products
  async loadInitialsProducts() {
    this.setState({ isLoading: true });
    let { page, size, sortBy, products, total, ads } = this.state;
    try {
      products = await this.getProducts(page, size, sortBy);
      total = await this.getProductsTotal();

      // Generating ads random number and save in state
      for (let i=0,j=0; i<=total+addsInterval; i+=addsInterval, j++) {
        let random = Math.floor(Math.random()*1000);
        // Prevent ads show twice
        while (ads.find((value) => value === random)) {
          random = Math.floor(Math.random()*1000);  
        }
        ads[j] = random;
      }
    } catch (e) {} 
    await this.setState({ products: products, total: total, ads: ads, isLoading: false });
  }

  // Get Products Total
  async getProductsTotal() {
    let response = await fetchHelper(`${apiUrl}/products?_page=1`,{
      method: "GET"
    });
    const total = response.headers.get('X-Total-Count');
    return total;
  }
  
  // Get Products
  async getProducts(page, size, sortBy) {
    let products = [];

    let response = await fetchHelper(`${apiUrl}/products?_page=${page}&_limit=${size}&_sort=${sortBy}`,{
      method: "GET"
    });

    response = await response.json();
    products = response.map(product => {
      let date = ProductPage.strToDate(product.date);
      return { ...product, date: date, price: product.price / 100 };
    });

    return products;
  }

  // Preemtive load
  async loadCache() {
    let { page, pageLoaded, size, sortBy, products, isLoadingCache } = this.state;

    if (pageLoaded <= page + 1 && !isLoadingCache) {
      await this.setState({ isLoadingCache: true });
      try {
        products = await this.getProducts(pageLoaded + 1, size, sortBy);
      } catch (e) {} 
      await this.setState((prevState, props) => {
        return { products: prevState.products.concat(products), pageLoaded: prevState.pageLoaded + 1, isLoadingCache: false };
      });
    }
  }

  async doSort(e) {
    let selected = e.target.value;
    await this.setState({ ...this.initalState, sortBy: selected });

    this.loadInitialsProducts();
  }

  // Track user scrolling, to implement infinite scroll loading
  trackScrolling = async () => {
    let { page, size, sortBy, total, hasMore, isLoadingCache } = this.state;

    // Check if user scroll to bottom, 20 px from bottom of page
    if (
        window.innerHeight + document.documentElement.scrollTop >= document.documentElement.offsetHeight &&
        hasMore
      ) {

        this.setState({ isLoading: true });
        hasMore = total >= page * size ? true : false;

        // Delay 1 second to show loading bar
        await sleep(1000);
        await this.setState((prevState, props) => {
          return { page: prevState.page + 1, isLoading: false, hasMore: hasMore };
        });
    }
  }

  render() {

    let { ads, page, size, products } = this.state;

    return (
      <div className="container is-fluid">
        <header>
          <h2 className="title is-2">Products Grid</h2>
          <p>Here you're sure to find a bargain on some of the finest ascii available to purchase. Be sure to peruse our selection of ascii faces in an exciting range of sizes and prices.</p>

          <div className="columns is-clearfix">
            <div className="column is-pulled-left">
              <label>But first, a word from our sponsors:</label>
            </div>
            <div className="column is-pulled-right">
              <div className="is-pulled-right">
                <label>Sort By</label> 
                <div className="select">
                  <select onChange={this.doSort} value={this.sortBy}>
                    {
                      this.sortOptions.map((option, key) => {
                        return (<option value={option.value} key={key}>{option.label}</option>)
                      })
                    }
                  </select>
                </div>
              </div>
            </div>
          </div>
        </header>
        <section className="columns is-multiline products">

          { 
            products && products.map((product, i) => {
              const productDate = product.date;
              let diffDay = ProductPage.diffDayOfDate(new Date(), productDate);
              let days = diffDay > 1 ? 'days' : 'day';
              let dateFormatted = diffDay < 4 ? diffDay + days + ' ago' : ProductPage.dateToStr(productDate);
              let adsTotal = parseInt(i/addsInterval, 10);
              let total = ((page * size) - adsTotal);
              let columnStyle = i <  total ? 'column is-one-fifth' : 'column is-one-fifth is-hidden';

              return (
                <React.Fragment key={i}>
                  { i % addsInterval === 0 && 
                    (
                      <div className={columnStyle}>
                        <div className="card ad-item">
                          <img src={'/ads/?r=' + this.state.ads[i/addsInterval] } alt="ads"/>
                        </div>
                      </div>
                    )
                  }
                  <div className={columnStyle}>
                    <div className="card product-item">
                      <div className="card-content">
                        <div className="content">
                          <div className="product-face">
                            <span style={{fontSize: product.size+'px'}}>{product.face}</span>
                          </div>
                          <div className="product-details is-clearfix">
                            <div className="price is-pulled-left">${product.price.toFixed(2)}</div>
                            <div className="date is-pulled-right">{dateFormatted}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>  
              )
            })
          }

        </section>
        {
          this.state.isLoading && 
            <div id="loading"></div>
        }
        {
          !this.state.hasMore && 
            <div id="no-more-message" className="notification is-warning">
              ~ End of catalogue ~
            </div>
        }
      </div>
    );
  }
}

ReactDOM.render(<ProductPage />, document.getElementById('root'));