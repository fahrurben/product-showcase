const toJson = (response) => response.json();

const handleErrors = (response) => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};

const fetchHelper = (url, options) => {
  options.headers = {
    'Content-Type': 'application/json'
  };
  return fetch(url,options);
}

const sleep = (time) => {
  return new Promise((resolve) => setTimeout(resolve, time));
}

export { 
  toJson, 
  handleErrors,
  fetchHelper,
  sleep
};